import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './Router/index'
import { store } from './store/index'


Vue.use(VueRouter)


const router=new VueRouter({
  routes:Routes,
  mode:'history'
})

new Vue({
  el: '#app',
  store:store,
  router,
  render: h => h(App)
})
