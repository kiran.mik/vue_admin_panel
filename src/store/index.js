import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export const store=new Vuex.Store({
    state:{
        editcms:{},
        editUser:{}
    },
    getters: {
        displayNames: state => {
          return state.editcms
        },
        displayUserNames:state=>{
            return state.editUser
        }
    },
    mutations:{
        editcms1:(state,payload)=>{
        state.editcms=payload
        },
        editUser1:(state,payload)=>{
            state.editUser=payload
            }
    },
    actions:{
        editcms2:(context,payload)=>{
            context.commit('editcms1',payload)
        },
        editUser2:(context,payload)=>{
            context.commit('editUser1',payload)
        }
    }
})