import Common from '../common'
import Dashboard from '../pages/components/dashboard'
import Login from '../pages/login'
import Cms from '../pages/components/cms/cmsMngmt'
import CmsAdd from '../pages/components/cms/cmsAdd'
import CmsEdit from '../pages/components/cms/cmsEdit'
import User from '../pages/components/user/userMngmt'
import Useradd from '../pages/components/user/userAdd'
import EditUser from '../pages/components/user/editUser'

export default [
  { path: '/', component: Login },
  { path: '/dash', component: Dashboard },
  { path: '/common', component: Common },
  { path: '/cmslist', component: Cms },
  { path: '/cmsAdd', component: CmsAdd },
  { path: '/Editcms', component: CmsEdit },
  { path: '/userlist', component: User },
  { path: '/userAdd', component: Useradd },
  { path: '/Edituser', component: EditUser }

]
